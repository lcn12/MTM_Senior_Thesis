ReadMe for MTM2017 spider data csv

collapsed field notes.csv : numeric identifier, transect, and orientation of spiders. A collapsed file from raw WV Spider Survey Field Notes excel file in MTM_SeHg Dropbox folder
spiderIDs.raw.csv : numeric identifier, site, site class, family, genus, sex, abodmen length, chelicera to abdomen length